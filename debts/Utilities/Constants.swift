//
//  Constants.swift
//  debts
//
//  Created by Artur on 01.08.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import Foundation

let TO_LOGIN = "toLogin"
let TO_DEBTS = "toDebts"

let USER_UID_KEY = "user_uid_key"
