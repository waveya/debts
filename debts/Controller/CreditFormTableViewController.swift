//
//  FormTableViewController.swift
//  debts
//
//  Created by Artur on 21.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class CreditFormTableViewController: UITableViewController {
    
    
    var user: UserStruct!
    var ref: DatabaseReference!
    var credits = [Credit]()
    
    private struct Property {
        static let unwindSegue = "unwindSegue"
    }
    
    var currency: Currency?
    var credit: Credit?
    var totalCredit: TotalCredit?
    var isDatePickerHidden = true
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var sumTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var currencyIconLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setToolbarHidden(true, animated: false)
        updateDateView()
        tableView.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        
        checkForCurrentUser()
        displayCredit()
        initView()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setToolbarHidden(false, animated: true)
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        updateDateView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let normalCellHeight = CGFloat(44)
        let largeCellHeight = CGFloat(216)
        
        switch indexPath {
        case [3,0]:
            return isDatePickerHidden ? normalCellHeight : largeCellHeight
        case [3,1]:
            return CGFloat(120)
        default: return normalCellHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        isDatePickerHiden(indexPath: indexPath)
        
    }
    
    func updateDateView() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        dateLabel.text = dateFormatter.string(from: datePicker.date)
    }
    
    func isDatePickerHiden(indexPath: IndexPath) {
        switch indexPath {
        case [3,0]:
            isDatePickerHidden = !isDatePickerHidden
            tableView.beginUpdates()
            tableView.endUpdates()
        default: break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard segue.identifier == Property.unwindSegue else { return }
        let name = nameTextField.text!
        guard let optionalSum = Int(sumTextField.text!) else  { return }
        var sum = optionalSum
        if segmentedControl.selectedSegmentIndex == 0 {
            sum = abs(sum)
        } else if segmentedControl.selectedSegmentIndex == 1 {
            if sum > 0 {
                sum = sum * (-1)
            }
        }
        let comment = textView.text ?? ""
        let date = datePicker.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        let dateString = dateFormatter.string(from: date)
        let currency = self.currency
        let defaultCurrency = Currency.loadSampleCurrencies()[0]
        
        credit = Credit(userId: user.uid, name: name, sum: sum, currency: currency ?? defaultCurrency, date: dateString, comment: comment)
        let creditRef = ref.child((credit?.name.lowercased())!)
        creditRef.setValue(credit?.convertToDictionary())
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        guard let sourceVC = segue.source as? CurrencyTableViewController else { return }
        currency = sourceVC.currency
        guard let currencyName = sourceVC.currency?.name else { return }
        guard let currencyImage = sourceVC.currency?.image else { return }
        currencyNameLabel.text = currencyName
        currencyIconLabel.text = currencyImage
    }
    
    func displayCredit() {
        
        guard let credit = credit else { return }
        
        nameTextField.text = credit.name
        let dateFormatter = DateFormatter()
        print("DATE!: \(credit.date)")
        dateFormatter.dateStyle = .long
        let date = dateFormatter.date(from: credit.date)
        dateLabel.text = credit.date
        sumTextField.text = "\(credit.sum)"
        datePicker.date = date!
        textView.text = credit.comment
        currencyNameLabel.text = credit.currency?.name
    }
    
    func checkForCurrentUser() {
        guard let currentUser = Auth.auth().currentUser else { return }
        user = UserStruct(user: currentUser)
        ref = Database.database().reference(withPath: "users").child(String(user.uid))
    }
    
    func initView() {
        tableView.tableFooterView = UIView(frame: .zero)
    }
    

}
