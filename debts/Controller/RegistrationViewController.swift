//
//  RegistrationViewController.swift
//  debts
//
//  Created by Artur on 01.08.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionLabel(isHiden: true)
        spinner.isHidden = true
    }
    @IBAction func createAccountTapped(_ sender: UIButton) {
        
        guard let email = emailTextField.text, emailTextField.text != "" else {
            descriptionLabel(isHiden: false)
            descriptionLabel.text = "Enter login"
            return }
        guard let password = passwordTextField.text, passwordTextField.text != "" else {
            descriptionLabel(isHiden: false)
            descriptionLabel.text = "Enter password"
            return }
        spinner.isHidden = false
        spinner.startAnimating()
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if user != nil {
                self.verificationEmail()
                print("successfull")

                let loginVC = LoginViewController()
                Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                    loginVC.checkIfUserExist(user, error)
                    loginVC.descriptionLabel(isHidden: true)
                }
                self.performSegue(withIdentifier: TO_DEBTS, sender: self)
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
            } else {
                print("registration error")
                let ac = UIAlertController(title: "ERROR", message: "ERROR", preferredStyle: .alert)
                let aa = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(aa)
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
                self.descriptionLabel(isHiden: false)
                if let myError = error?.localizedDescription {
                    print(myError)
                    if myError == "The email address is badly formatted." {
                        self.descriptionLabel.text = "The email address is badly formatted."
                    } else if myError == "The password must be 6 characters long or more." {
                        self.descriptionLabel.text = "The password must be 6 characters long or more."
                    } else {
                        self.present(ac, animated: true, completion: nil)
                    }
                } else {
                    print("ERROR")
                }
            }
        }
    }
    
    func verificationEmail() {
        Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
            if error != nil {
                print(error?.localizedDescription ?? "89")
            }
        })
    }
    
    func descriptionLabel(isHiden: Bool) {
        descriptionLabel.isHidden = isHiden
    }
}
