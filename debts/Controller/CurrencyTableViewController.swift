//
//  CurrencyTableViewController.swift
//  debts
//
//  Created by Artur on 23.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit

class CurrencyTableViewController: UITableViewController {

    struct Property {
        static let currencyCellIdentifier = "currencyCell"
        static let unwindToCurrencyForm = "unwindToCurrencyForm"
    }
    
    var currencies: [Currency] = []
    var currency: Currency?

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return currencies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Property.currencyCellIdentifier, for: indexPath) as! CurrencyCell
        let indexOfElement = indexPath.row
        
        cell.imageLabel.text = currencies[indexOfElement].image
        cell.nameLabel.text = currencies[indexOfElement].name
        cell.descriptionLabel.text = currencies[indexOfElement].description
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.currencies.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        currency = currencies[indexPath.row]
        performSegue(withIdentifier: Property.unwindToCurrencyForm, sender: nil)
    }
    
    func initView() {
        if let savedCurrencies = Currency.loadCurrencies() {
            currencies = savedCurrencies
        } else {
            currencies = Currency.loadSampleCurrencies()
        }
    }
}










