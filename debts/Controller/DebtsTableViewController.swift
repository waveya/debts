//
//  DebtsTableViewController.swift
//  debts
//
//  Created by Artur on 21.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth
import FirebaseDatabase
import Firebase


class DebtsTableViewController: UITableViewController {
    
    struct Property {
        static let creditCellIdentifier = "creditCell"
        static let toEditVCSegue = "editSegue"
        static let unwindSegue = "unwindSegue"
        static let totalCellIdentifier = "totalCell"
    }
    var ref: DatabaseReference?
    var user: UserStruct!
    var curency: Currency?
    var credits = [Credit]()
    
    var credit: Credit?
    var totalCell: TotalCreditCell?
    var totalCredit: TotalCredit?
    var totalDictionary: [String: Int] = [:]
    var sum = 0
    
    let loadingView = UIView()
    let spinner = UIActivityIndicatorView()
    let loadingLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setLoadingScreen()
        super.viewWillAppear(animated)
        guard let currentUser = Auth.auth().currentUser else { return }
        print("LOGGED IN, GETTING DATA")
        user = UserStruct(user: currentUser)
        ref = Database.database().reference(withPath: "users").child(String(user.uid))
        ref?.observe(.value) { [weak self] (snapshot) in
            var _credits = [Credit]()
            for item in snapshot.children {
                let credit = Credit(snapshot: item as! DataSnapshot)
                _credits.append(credit)
                print(credit.name)
            }
            print("DATA HAS GOTTEn")
            self?.credits = _credits
            self?.tableView.reloadData()
            self?.removeLoadingScreen()
        }
        refreshTotalLabel()
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return credits.count
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let creditCell = tableView.dequeueReusableCell(withIdentifier: Property.creditCellIdentifier, for: indexPath) as! CreditCell
            
            let indexOfElement = indexPath.row
            let credit = credits[indexOfElement]
            creditCell.nameLabel.text = credit.name
//            creditCell.currencySymbolLabel.text = credit.currency?.symbol
            creditCell.currencySymbolLabel.text = Symbol.uah.rawValue
            totalCell?.totalCurrencyLabel.text = Symbol.uah.rawValue
            if credit.sum > 0 {
                creditCell.currencyAmountLabel.text = "+\(credit.sum)"
            } else {
                creditCell.currencyAmountLabel.text = "\(credit.sum)"
            }
            
        
            totalCell?.totalLabel.text = "\(refreshTotalLabel())"
            setDebtColor(credit, creditCell)
            setTotalDebtColor()
            hideSeparator()
            return creditCell
        } else {
            totalCell = tableView.dequeueReusableCell(withIdentifier: Property.totalCellIdentifier, for: indexPath) as? TotalCreditCell
            if sum > 0 {
                totalCell?.totalLabel.text = "+\(sum)"
            } else {
                totalCell?.totalLabel.text = "\(sum)"
            }
            hideSeparator()
            hideTotalLabel()
            setTotalDebtColor()
            return totalCell!
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: Property.toEditVCSegue, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            removeFromFirebase(indexPath.row)
            self.sum -= credits[indexPath.row].sum
            self.credits.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            refreshTotalLabel()
        }
    }
    
    @IBAction func logoutTapped(_ sender: UIBarButtonItem) {
        confirmationForExit()
    }
    
    @IBAction func unwindToCreditList(segue: UIStoryboardSegue) {
        guard let sourceViewController = segue.source as? CreditFormTableViewController else { return }
        if let credit = sourceViewController.credit {
            let newIndexPath = IndexPath(row: credits.count, section: 0)
            credits.append(credit)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
        guard let currencyName = sourceViewController.currency else { return }
        self.curency = currencyName
    }
    
    func removeFromFirebase(_ index: Int) {
        Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).child((credits[index].name.lowercased())).removeValue { (error, refer) in
            if error != nil {
                print(error as Any)
            } else {
                print(refer)
                print("Child Removed Correctly")
            }
        }
    }
    
    @discardableResult //remove -> Result of call to [myFunction] is unused
    func refreshTotalLabel() -> Int {
        sum = 0
        for credit in credits {
            sum += credit.sum
        }
        return sum
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier == Property.toEditVCSegue else { return }
        let creditForm = segue.destination as! CreditFormTableViewController
        let indexPath = tableView.indexPathForSelectedRow!
        let selectedCredit = credits[indexPath.row]
        creditForm.credit = selectedCredit
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func hideTotalLabel() {
        if sum == 0 {
            totalCell?.isHidden = true
        } else {
            totalCell?.isHidden = false
        }
    }
    
    func confirmationForExit() {
        
        let ac = UIAlertController(title: "Завершить сеанс?", message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Выйти", style: .default) { (action) in
            do {
                if Auth.auth().currentUser != nil {
                    try Auth.auth().signOut()
                    // Remove User Session from device
                    UserDefaults.standard.removeObject(forKey: USER_UID_KEY)
                    UserDefaults.standard.synchronize()
                    self.dismiss(animated: true, completion: nil)
                    print("logedout")
                }
            } catch let signOutError as NSError {
                print("ERROR EXIT")
                print(signOutError)
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .default, handler: nil)
        ac.addAction(cancelAction)
        ac.addAction(confirmAction)
        present(ac, animated: true)
    }
    
    private func setLoadingScreen() {
        
        // Sets the view which contains the loading text and the spinner
        let width: CGFloat = 120
        let height: CGFloat = 30
        let x = (tableView.frame.width / 2) - (width / 2)
        let y = (tableView.frame.height / 2) - (height / 2) - (navigationController?.navigationBar.frame.height)!
        loadingView.frame = CGRect(x: x, y: y, width: width, height: height)
        tableView.separatorColor = .clear
        
        // Sets loading text
        loadingLabel.textColor = .gray
        loadingLabel.textAlignment = .center
        loadingLabel.text = "Loading..."
        loadingLabel.frame = CGRect(x: 0, y: 0, width: 140, height: 30)
        
        // Sets spinner
        spinner.style = .gray
        spinner.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        spinner.startAnimating()
        
        // Adds text and spinner to the view
        loadingView.addSubview(spinner)
        loadingView.addSubview(loadingLabel)
        
        self.tableView.addSubview(loadingView)
        print("asdad")
        
    }
    
    private func removeLoadingScreen() {
        
        // Hides and stops the text and the spinner
        spinner.stopAnimating()
        spinner.isHidden = true
        loadingLabel.isHidden = true
        tableView.separatorColor = .gray
    }
    
    func hideSeparator() {
        
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    func setDebtColor(_ credit: Credit, _ creditCell: CreditCell) {
        
        if credit.sum < 0 {
            creditCell.currencyAmountLabel.textColor = .red
            creditCell.currencySymbolLabel.textColor = .red
        } else {
            creditCell.currencyAmountLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            creditCell.currencySymbolLabel.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
    }
    
    func setTotalDebtColor() {
        
        if sum < 0 {
            totalCell?.totalLabel.textColor = .red
            totalCell?.totalCurrencyLabel.textColor = .red
        } else {
            totalCell?.totalLabel.textColor =  #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            totalCell?.totalCurrencyLabel.textColor =  #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
    }
}
