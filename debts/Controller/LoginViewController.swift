//
//  LoginViewController.swift
//  debts
//
//  Created by Artur on 01.08.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        descriptionLabel(isHidden: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.object(forKey: USER_UID_KEY) != nil {
            performSegue(withIdentifier: TO_DEBTS, sender: nil)
            print("HELLO")
        }
    }

    @IBAction func loginTapped(_ sender: UIButton) {
        login()
    }
    
    @IBAction func forgotPasswordTapped(_ sender: UIButton) {

        guard let email = emailTextField.text, email != "" else {
            descriptionLabel(isHidden: false)
            descriptionLabel.text = "To reset password, enter email"
            print("RESET PASSWORD")
            return }
        descriptionLabel(isHidden: true)
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            
        }
    }
    
    func login() {
        guard let email = emailTextField.text, email != "" else { return }
        guard let password = passwordTextField.text, password != "" else { return }
        spinner.isHidden = false
        spinner.startAnimating()
    
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            self.checkIfUserExist(user, error)
            self.spinner.isHidden = true
            self.spinner.stopAnimating()

        }

    }
    
    func checkIfUserExist(_ user: AuthDataResult?, _ error: Error?) {
        if user != nil {
            UserDefaults.standard.set(Auth.auth().currentUser!.uid, forKey: USER_UID_KEY)
            UserDefaults.standard.synchronize()
            print("successfull")
            descriptionLabel(isHidden: true)
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
            self.performSegue(withIdentifier: TO_DEBTS, sender: nil)
        } else {
            let ac = UIAlertController(title: "ERROR", message: "ERROR", preferredStyle: .alert)
            let aa = UIAlertAction(title: "OK", style: .default, handler: nil)
            ac.addAction(aa)
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            descriptionLabel(isHidden: false)
            if let myError = error?.localizedDescription {
                if myError == "The email address is badly formatted." {
                    descriptionLabel.text = "The email address is badly formatted."
                } else if myError == "The password is invalid or the user does not have a password." {
                    descriptionLabel.text = "Incorrect password"
                } else if myError == "There is no user record corresponding to this identifier. The user may have been deleted." {
                    descriptionLabel.text = "No user with such email"
                } else {
                    self.present(ac, animated: true, completion: nil)
                }
                print(myError)
            } else {
                print("ERROR")
            }
        }
        
    }
    
    func descriptionLabel(isHidden: Bool) {
        descriptionLabel.isHidden = isHidden
    }
    
    @IBAction func unwind(segue: UIStoryboardSegue) {
    
    }
}
