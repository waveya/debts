//
//  CreditCell.swift
//  debts
//
//  Created by Artur on 22.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit

class CreditCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currencySymbolLabel: UILabel!
    @IBOutlet weak var currencyAmountLabel: UILabel!
    
}
