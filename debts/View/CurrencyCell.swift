//
//  CurrencyCell.swift
//  debts
//
//  Created by Artur on 23.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
