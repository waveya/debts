//
//  TotalCreditCell.swift
//  debts
//
//  Created by Artur on 24.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import UIKit

class TotalCreditCell: UITableViewCell {

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalCurrencyLabel: UILabel!
    
}
