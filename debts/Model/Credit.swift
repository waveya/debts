//
//  Credit.swift
//  debts
//
//  Created by Artur on 21.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

struct Credit {
    
    var userId: String
    var ref: DatabaseReference?
    var name: String
    var sum: Int
    var currency: Currency?
    var date: String
    var comment: String
    
    init(userId: String, name: String, sum: Int, currency: Currency, date: String, comment: String) {

        self.name = name
        self.sum = sum
        self.currency = currency
        self.date = date
        self.comment = comment
        self.userId = userId
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {//срез данных?
        let snapshotValue = snapshot.value as! [String: AnyObject]
        comment = snapshotValue["comment"] as! String
        currency = snapshotValue["currency"] as? Currency
        date = snapshotValue["date"] as! String
        name = snapshotValue["name"] as! String
        sum = snapshotValue["sum"] as! Int

        userId = snapshotValue["userId"] as! String
        ref = snapshot.ref
    }
    


    func convertToDictionary() -> Any {
        return ["userId": userId, "name":name, "sum": sum, "currency": currency!.name, "date": date, "comment": comment ]
    }
}






