//
//  Currency.swift
//  debts
//
//  Created by Artur on 24.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import Foundation

struct Currency: Equatable {
    var name: String
    var image: String
    var description: String
    var symbol: String?
    
    static func loadSampleCurrencies() -> [Currency] {
        let currency1 = Currency(name: "UAH", image: "🇺🇦", description: "ukrainian currency", symbol: "₴")
        let currency2 = Currency(name: "USD", image: "🇺🇸", description: "american currency", symbol: "$")
        let currency3 = Currency(name: "EUR", image: "🇪🇺", description: "european currency", symbol: "€")
        return [currency1,currency2,currency3]
    }
    
    static func loadCurrencies() -> [Currency]? {
        return nil
    }
}

func ==(lhs: Currency, rhs: Currency) -> Bool {
    return lhs.symbol == rhs.symbol
}

enum Symbol: String {
    case uah = "₴", usd = "$", eur = "€"
}
