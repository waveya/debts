//
//  User.swift
//  debts
//
//  Created by Artur on 06.08.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import Foundation
import Firebase

struct UserStruct {
    let uid: String
    let email: String
    
    init(user: User) {
        self.uid = user.uid
        self.email = user.email!
    }
}
