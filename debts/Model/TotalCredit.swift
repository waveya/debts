//
//  TotalCredit.swift
//  debts
//
//  Created by Artur on 24.07.2018.
//  Copyright © 2018 Artur Sakhno. All rights reserved.
//

import Foundation

struct TotalCredit {
    var total: Int
    var symbol: String
}
